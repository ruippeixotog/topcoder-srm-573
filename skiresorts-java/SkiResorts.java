
public class SkiResorts {

	public long minCost(String[] road, int[] altitude) {
		long[] distance = new long[road.length];
		int[] pred = new int[road.length];
		
		distance[0] = 0;
		pred[0] = -1;
		for (int i = 1; i < road.length; i++) {
			distance[i] = Long.MAX_VALUE;
			pred[i] = -1;
		}
		
		for (int i = 0; i < road.length; i++) {
			for (int u = 0; u < road.length; u++) {
				for (int v = 0; v < road.length; v++) {
					if (road[u].charAt(v) == 'Y'
							&& distance[u] != Long.MAX_VALUE
							&& distance[u] + weight(altitude[u], altitude[v]) < distance[v]) {
						distance[v] = distance[u]
								+ weight(altitude[u], altitude[v]);
						pred[v] = u;
					}
				}
			}
		}
		return distance[road.length - 1] == Long.MAX_VALUE ? -1
				: distance[road.length - 1];
	}

	long weight(int from, int to) {
		return from >= to ? 0 : to - from;
	}
}
