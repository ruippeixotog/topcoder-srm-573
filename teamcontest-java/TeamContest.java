import java.util.Arrays;

public class TeamContest {

	public int worstRank(int[] strength) {
		int myRank = Math.max(strength[0], Math.max(strength[1], strength[2]))
				+ Math.min(strength[0], Math.min(strength[1], strength[2]));
		
		int[] others = new int[strength.length - 3];
		for (int i = 3; i < strength.length; i++) {
			others[i - 3] = strength[i];
		}
		Arrays.sort(others);

		int bestCounter = 0;
		int worst = 0, best = others.length - 1;
		while (worst < best) {
			if (others[worst] + others[best] <= myRank) {
				worst += 3;
			} else {
				bestCounter++;
				worst += 2;
				best--;
			}
		}
		return 1 + bestCounter;
	}
}
